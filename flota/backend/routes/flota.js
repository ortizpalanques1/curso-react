const router = require('express').Router();
let Flota = require('../model/flota.model');

router.route('/').get((req, res) => {
    Flota.find()
        .then(flota => res.json(flota))
        .catch(err => res.status(400).json('Error: ' + err));
});

router.route('/add').post((req, res) => {
    const name = req.body.name;
    const boat = req.body.boat;
    const fleet = req.body.fleet;
    const ton = Number(req.body.ton);
    const crew = Number(req.body.crew);
    const sail = Date.parse(req.body.sail);
    const location = [];
    const crewmembers = [];

    const newFlota = new Flota({
        name,
        boat,
        fleet,
        ton,
        crew,
        sail,
        location,
        crewmembers,
    });

    newFlota.save()
        .then(() => res.json('Flota added!'))
        .catch(err => res.status(400).json('Error: ' + err));
});

router.route('/:id').get((req, res) => {
    Flota.findById(req.params.id)
        .then(flota => res.json(flota))
        .catch(err => res.status(400).json('Error: ' + err));
});

router.route('/:id').delete((req, res) => {
    Flota.findByIdAndDelete(req.params.id)
        .then(() => res.json('Buque borrado.'))
        .catch(err => res.status(400).json('Error: ' + err));
});

router.route('/update/:id').post((req, res) => {
    Flota.findById(req.params.id)
        .then(flota => {
            flota.name = req.body.name;
            flota.boat = req.body.boat;
            flota.fleet = req.body.fleet;
            flota.ton = Number(req.body.ton);
            flota.crew = Number(req.body.crew);
            flota.sail = Date.parse(req.body.sail);
            flota.location = [];
            flota.crewmembers = [];

            flota.save()
                .then(() => res.json('Buque guardado'))
                .catch(err => res.status(400).json('Error: ' + err));
        })
        .catch(err => res.status(400).json('Error: ' + err))
});

module.exports = router;