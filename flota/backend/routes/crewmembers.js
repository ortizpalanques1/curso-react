const router = require('express').Router();
let Crewmember = require('../model/crewmembers.model');

router.route('/').get((req, res) => {
    Crewmember.find()
        .then(crewmember => res.json(crewmember))
        .catch(err => res.status(400).json('Error: ' + err));
});

router.route('/add').post((req, res) => {
    const name = req.body.name;
    const position = req.body.position;
    const birthdate = Date.parse(req.body.birthdate);

    const newCrewmember = new Crewmember({
        name,
        position,
        birthdate,
    });

    newCrewmember.save()
        .then(() => res.json('Crewmember added!'))
        .catch(err => res.status(400).json('Error: ' + err));
});

router.route('/:id').get((req, res) => {
    Crewmember.findById(req.params.id)
        .then(crewmember => res.json(crewmember))
        .catch(err => res.status(400).json('Error: ' + err));
});

router.route('/:id').delete((req, res) => {
    Crewmember.findByIdAndDelete(req.params.id)
        .then(() => res.json('Tripulante borrado.'))
        .catch(err => res.status(400).json('Error: ' + err));
});

router.route('/update/:id').post((req, res) => {
    Crewmember.findById(req.params.id)
        .then(crewmember => {
            crewmember.name = req.body.name;
            crewmember.position = req.body.position;
            crewmember.birthdate = Date.parse(req.body.birthdate);

            crewmember.save()
                .then(() => res.json('Tripulante guardado'))
                .catch(err => res.status(400).json('Error: ' + err));
        })
        .catch(err => res.status(400).json('Error: ' + err))
});


module.exports = router;