const mongoose=require('mongoose');

const Schema=mongoose.Schema;

const crewmemberSchema=new Schema({
    name:{
        type: String,
        required: true,
        unique: true,
        trim: true,
        minlength: 3
    },
    position:{
        type: String,
    },
    birthdate:{
        type: Date,
    },
}, {
    timestamps: true,
});

const Crewmember=mongoose.model('Crewmember',crewmemberSchema);

module.exports=Crewmember;