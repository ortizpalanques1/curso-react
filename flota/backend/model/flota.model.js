const mongoose=require('mongoose');

const Schema=mongoose.Schema;

const flotaSchema=new Schema({
    name:{
        type: String,
        required: true,
        unique: true,
        trim: true,
        minlength: 3
    },
    boat:{
        type: String,
    },
    fleet:{
        type: String,
    },
    ton: {
        type: Number,
    },
    crew:{
        type: Number,
    },
    crewmembers:{
        type:[],
    },
    sail:{
        type: Date,
    },
    location:{
        type: {type:String},
        coordinates:[Number],
    },
}, {
    timestamps: true,
});

const Flota=mongoose.model('Flota',flotaSchema);

module.exports=Flota;