import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

const Crewmember = props => (
    <tr>
        <td>{props.crewmember.name}</td>
        <td>{props.crewmember.position}</td>
        <td>{props.crewmember.birthdate.substring(0,10)}</td>
        <td>
            <Link to={"/edit1/" + props.crewmember._id}>edit</Link> | <a href="#" onClick={() => { props.deleteCrewmember(props.crewmember._id) }}>delete</a>
        </td>
    </tr>
)

export default class CrewmemberList extends Component {
    constructor(props) {
        super(props);

        this.deleteCrewmember = this.deleteCrewmember.bind(this);

        this.state = { crewmember: [] };
    }

    componentDidMount() {
        axios.get('http://localhost:5000/crewmembers/')
            .then(response => {
                this.setState({ crewmember: response.data })
            })
            .catch((error) => {
                console.log(error);
            })
    }

    deleteCrewmember(id) {
        axios.delete('http://localhost:5000/crewmembers/' + id)
            .then(res => console.log(res.data));

        this.setState({
            crewmember: this.state.crewmember.filter(el => el._id !== id)
        })
    }

    CrewmemberList() {
        return this.state.crewmember.map(crewmemberexistente => {
            return <Crewmember crewmember={crewmemberexistente} deleteCrewmember={this.deleteCrewmember} key={crewmemberexistente._id} />;
        })
    }

    render() {
        return (
            <div>
                <h3>Listado de Tripulantes</h3>
                <table className="table">
                    <thead className="thead-light">
                        <tr>
                            <th>Nombre</th>
                            <th>Posición</th>
                            <th>Nacimiento</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.CrewmemberList()}
                    </tbody>
                </table>
            </div>
        )
    }
}