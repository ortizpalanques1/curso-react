import React, {Component} from 'react';
import axios from 'axios';
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";

export default class EditCrewmember extends Component{
    constructor(props){
        super(props);

        this.onChangeName=this.onChangeName.bind(this);
        this.onChangePosition=this.onChangePosition.bind(this);
        this.onChangeBirthdate=this.onChangeBirthdate.bind(this);
        this.onSubmit=this.onSubmit.bind(this);


        this.state={
            name:'',
            position:'',
            birthdate: new Date()
        }        
    }

    componentDidMount(){
        axios.get('http://localhost:5000/crewmembers/'+this.props.match.params.id)
            .then(response=>{
                this.setState({
                    name: response.data.name,
                    position: response.data.position,
                    birthdate: new Date(response.data.birthdate)
                })
            })
            .catch(function (error){
                console.log(error);
            })
    }

    onChangeName(e){
        this.setState({
            name: e.target.value
        });
    }

    onChangePosition(e){
        this.setState({
            position: e.target.value
        });
    }

    onChangeBirthdate(date){
        this.setState({
            birthdate: date
        });
    }

    onSubmit(e){
        e.preventDefault();

        const crewmember={
            name: this.state.name,
            position: this.state.position,
            birthdate: this.state.birthdate
        }

        console.log(crewmember);

        axios.post('http://localhost:5000/crewmembers/update/'+this.props.match.params.id, crewmember)
            .then(res=>console.log(res.data));
    }

    render(){
        return(
            <div>
                <h3>Editar  Marino</h3>
                <form onSubmit={this.onSubmit}>
                    <div className="form-group">
                        <label>Nombre</label>
                        <input type="text"
                            required
                            className="form-control"
                            value={this.state.name}
                            onChange={this.onChangeName}
                        />
                    </div>
                    <div className="form-group">
                        <label>Posición</label>
                        <input type="text"
                            required
                            className="form-control"
                            value={this.state.position}
                            onChange={this.onChangePosition}
                        />
                    </div>
                    <div className="form-group">
                        <label>Nacimiento</label>
                        <DatePicker
                        selected={this.state.birthdate}
                        onChange={this.onChangeBirthdate}
                        />
                    </div>
                    <div className="form-group">
                        <input type="submit" value="Editar Marino" className="btn btn-primary"/>
                    </div>
                </form>
            </div>
        )
    }
}