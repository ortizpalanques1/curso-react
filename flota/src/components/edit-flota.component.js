import React, {Component} from 'react';
import axios from 'axios';
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";

export default class EditFlota extends Component{
    constructor(props){
        super(props);

        this.onChangeName=this.onChangeName.bind(this);
        this.onChangeBoat=this.onChangeBoat.bind(this);
        this.onChangeFleet=this.onChangeFleet.bind(this);
        this.onChangeTon=this.onChangeTon.bind(this);
        this.onChangeCrew=this.onChangeCrew.bind(this);
        this.onChangeSail=this.onChangeSail.bind(this);
        this.onSubmit=this.onSubmit.bind(this);


        this.state={
            name:'',
            boat:'',
            fleet:'',
            ton:0,
            crew:0,
            sail: new Date()
        }        
    }

    componentDidMount(){
        axios.get('http://localhost:5000/flota/'+this.props.match.params.id)
            .then(response=>{
                this.setState({
                    name: response.data.name,
                    boat: response.data.boat,
                    fleet: response.data.fleet,
                    ton: response.data.ton,
                    crew: response.data.crew,
                    sail: new Date(response.data.sail)
                })
            })
            .catch(function (error){
                console.log(error);
            })
    }

    onChangeName(e){
        this.setState({
            name: e.target.value
        });
    }

    onChangeBoat(e){
        this.setState({
            boat: e.target.value
        });
    }

    onChangeFleet(e){
        this.setState({
            fleet: e.target.value
        });
    }

    onChangeTon(e){
        this.setState({
            ton: e.target.value
        });
    }

    onChangeCrew(e){
        this.setState({
            crew: e.target.value
        });
    }

    onChangeSail(date){
        this.setState({
            sail: date
        });
    }

    onSubmit(e){
        e.preventDefault();

        const flota={
            name: this.state.name,
            boat: this.state.boat,
            fleet: this.state.fleet,
            ton: this.state.ton,
            crew: this.state.crew,
            sail: this.state.sail
        }

        console.log(flota);

        axios.post('http://localhost:5000/flota/update/'+this.props.match.params.id, flota)
            .then(res=>console.log(res.data));
    }

    render(){
        return(
            <div>
                <h3>Editar  Buque</h3>
                <form onSubmit={this.onSubmit}>
                    <div className="form-group">
                        <label>Name</label>
                        <input type="text"
                            required
                            className="form-control"
                            value={this.state.name}
                            onChange={this.onChangeName}
                        />
                    </div>
                    <div className="form-group">
                        <label>Tipo de Buque</label>
                        <input type="text"
                            required
                            className="form-control"
                            value={this.state.boat}
                            onChange={this.onChangeBoat}
                        />
                    </div>
                    <div className="form-group">
                        <label>Flota</label>
                        <input type="text"
                            required
                            className="form-control"
                            value={this.state.fleet}
                            onChange={this.onChangeFleet}
                        />
                    </div>
                    <div className="form-group">
                        <label>Tonelaje</label>
                        <input type="text"
                            required
                            className="form-control"
                            value={this.state.ton}
                            onChange={this.onChangeTon}
                        />
                    </div>
                    <div className="form-group">
                        <label>Tripulates</label>
                        <input type="text"
                            required
                            className="form-control"
                            value={this.state.crew}
                            onChange={this.onChangeCrew}
                        />
                    </div>
                    <div className="form-group">
                        <label>Botadura</label>
                        <DatePicker
                        selected={this.state.sail}
                        onChange={this.onChangeSail}
                        />
                    </div>
                    <div className="form-group">
                        <input type="submit" value="Editar Buque" className="btn btn-primary"/>
                    </div>
                </form>
            </div>
        )
    }
}