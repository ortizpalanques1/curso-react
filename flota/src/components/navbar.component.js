import React, {Component} from 'react';
import {Link} from 'react-router-dom';

export default class Navbar extends Component{

    render() {
        return(
            <nav className="navbar navbar-dark bg-dark navbar-expand-lg">
                <Link to = "/" className="navbar-brand" >Flotas</Link>
                <div className="collpase navbar-collapse">
                    <ul className="navbar-nav mr-auto">
                        <li className="navbar/item">
                            <Link to="/" className="nav-link">Flota</Link>
                        </li>
                        <li className="navbar/item">
                            <Link to="/create" className="nav-link">Crear BuqueFlota</Link>
                        </li>
                        <li className="navbar/item">
                            <Link to="/crewmember" className="nav-link">Crear Marino</Link>
                        </li>
                        <li className="navbar/item">
                            <Link to="/crew" className="nav-link">Marinos</Link>
                        </li>
                    </ul>
                </div>
            </nav>
        )
    }
}