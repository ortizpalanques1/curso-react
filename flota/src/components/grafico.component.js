import React, {Component} from 'react';
import Plot from 'react-plotly';
class Grafico extends React.Component{
    render(){
        return(
            <Plot 
                data={[
                    {
                        x:[1,2,3],
                        y:[2,6,3],
                        type: 'scatter',
                        mode: 'lines+points',
                        marker: {color: 'red'}
                    },
                ]}
                layout={{width: 300, height: 240,
                        title: 'El mar'
                    }}
            />
        )
    }
}