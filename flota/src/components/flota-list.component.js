import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

const Flota = props => (
    <tr>
        <td>{props.flota.name}</td>
        <td>{props.flota.boat}</td>
        <td>{props.flota.fleet}</td>
        <td>{props.flota.ton}</td>
        <td>{props.flota.crew}</td>
        <td>{props.flota.sail.substring(0,10)}</td>
        <td>
            <Link to={"/edit/" + props.flota._id}>edit</Link> | <a href="#" onClick={() => { props.deleteFlota(props.flota._id) }}>delete</a>
        </td>
    </tr>
)

export default class FlotaList extends Component {
    constructor(props) {
        super(props);

        this.deleteFlota = this.deleteFlota.bind(this);

        this.state = { flota: [] };
    }

    componentDidMount() {
        axios.get('http://localhost:5000/flota/')
            .then(response => {
                this.setState({ flota: response.data })
            })
            .catch((error) => {
                console.log(error);
            })
    }

    deleteFlota(id) {
        axios.delete('http://localhost:5000/flota/' + id)
            .then(res => console.log(res.data));

        this.setState({
            flota: this.state.flota.filter(el => el._id !== id)
        })
    }

    FlotaList() {
        return this.state.flota.map(flotaexistente => {
            return <Flota flota={flotaexistente} deleteFlota={this.deleteFlota} key={flotaexistente._id} />;
        })
    }

    render() {
        return (
            <div>
                <h3>Listado de Buques</h3>
                <table className="table">
                    <thead className="thead-light">
                        <tr>
                            <th>name</th>
                            <th>boat</th>
                            <th>fleet</th>
                            <th>ton</th>
                            <th>crew</th>
                            <th>sail</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.FlotaList()}
                    </tbody>

                </table>
            </div>
        )
    }
}