import React from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";

import Navbar from "./components/navbar.component";
import FlotaList from "./components/flota-list.component";
import EditFlota from "./components/edit-flota.component";
import CreateFlota from "./components/create-flota.component";
import CreateCrewmember from "./components/create-crewmember.component";
import CrewmemberList from "./components/crewmember-list.component";
import EditCrewmember from "./components/edit-crewmember.component"

function App() {
  return (
    <Router>
      <div className="container">
        <Navbar />
        <br />
        <Route path="/" exact component={FlotaList} />
        <Route path="/edit/:id" component={EditFlota} />
        <Route path="/create" component={CreateFlota} />
        <Route path="/crewmember" component={CreateCrewmember} />
        <Route path="/crew" component={CrewmemberList} />
        <Route path="/edit1/:id" component={EditCrewmember} />
        
      </div>
    </Router>
  );
}

export default App;
